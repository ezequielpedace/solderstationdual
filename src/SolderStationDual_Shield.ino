#include <Arduino.h>

/*
    SolderStation v0.2
    fngStudios
    26/07/2016
    Ezequiel Pedace

    Change log:

    v0.2 25/08/2016
    -Detect TC errors
    -Auto Off timer, configurable.

    v0.1 26/07/2016
    First Version



 */
//-------------------------------------------------------------------------------------
//    Includes
#include <SPI.h>
#include "Adafruit_MAX31855.h"
#include <Wire.h>
#include <LiquidCrystal.h>
#include <EEPROM.h>
#include <ClickEncoder.h>
#include <TimerOne.h>
#include <PID_v1.h>
#include <digitalWriteFast.h>
//-------------------------------------------------------------------------------------
//    Defines
#define CLICK               1
#define DBLCLICK            2
#define RELEASED            3
#define HELD                4
#define OPEN                0
#define ZXCYCLE             100
#define DINRED              2
#define AIR_ENC_B           18
#define AIR_ENC_A           19
#define TIP_ENC_A           20
#define TIP_ENC_B           21
#define AIR_ENC_BUT         A3
#define TIP_ENC_BUT         A5
#define PUSH                A4
#define AIR_HEAT_OUT        47
#define AIR_SPEED_OUT       45
#define TIP_HEAT_OUT        49
#define CS_AIR              53
#define CS_TIP              23

#define DEFAULT_IDLE_TEMP   100
#define MAX_IDLE_TEMP       150
#define MIN_IDLE_TEMP       80
#define DEFAULT_TIP_WORK_TEMP   220
#define MAX_TIP_WORK_TEMP       350
#define MIN_TIP_WORK_TEMP       180
#define DEFAULT_AIR_COOL_TEMP   100
#define DEFAULT_AIR_COOL_TIME   5    //minutos
#define DEFAULT_AIR_WORK_TEMP   300
#define MAX_AIR_WORK_TEMP       400
#define MIN_AIR_WORK_TEMP       200
#define DEFAULT_AIR_SPEED       40
#define MIN_AIR_SPEED           30
#define MAX_AIR_SPEED           100
#define MAX_KP              100
#define MIN_KP              2
#define DEFAULT_KP          10
#define MAX_KI              3.9
#define MIN_KI              0.0001
#define DEFAULT_KI          0.001
#define MAX_KD              30
#define MIN_KD              0
#define DEFAULT_KD          5
#define VALID_CONFIG        'y'
#define CONFIG_EEPROM_ADDR  30
//-------------------------------------------------------------------------------------
//    Variables de programa

unsigned int AirWorkSetpoint;
unsigned int TipWorkSetpoint;
unsigned char AirSpeed;
float Tip_Temperature;
float Air_Temperature;
double Tip_Output;
double Tip_Input;
double Tip_setPoint;
double Air_Output;
double Air_Input;
double Air_setPoint;

unsigned char Tip_Estado = 0;  //0:OFF 1:Working 2:Menu 3:IDLE
unsigned char Air_Estado = 0;  //0:OFF 1:Working 2:Menu 3:AIRSPEED
unsigned char Cur_Item = 0;
char Cur_Pos = 0;
char Menu_Page = 0;
int Tip_Enc_Last; 
int Tip_Enc_Value;
int Air_Enc_Last;
int Air_Enc_Value;
String temp;
unsigned long TimeStamp = 0;
volatile unsigned char ZXTicks = 0;
volatile unsigned char AirTicks = 0;
volatile unsigned char RotaryTicks = 0;

//-------------------------------------------------------------------------------------
//   Estructura de configuracion

 struct Configuration_T {
  unsigned char TIP_IDLE_T;
  unsigned char TIP_DEFAULT_T;
  unsigned char AIR_DEFAULT_T;
  unsigned char AIR_DEFAULT_S;
  double TIPKP;
  double TIPKI;
  double TIPKD;
  double AIRKP;
  double AIRKI;
  double AIRKD;
  byte VALID; // Ultimo, para comprobaciones de integridad.
} Current_Config;


//-------------------------------------------------------------------------------------
//    Constructores

LiquidCrystal lcd(12, 11, 10, 9, 8, 7);
Adafruit_MAX31855 TIP_TC(CS_TIP);
Adafruit_MAX31855 AIR_TC(CS_AIR);


ClickEncoder *TIP_Encoder;
ClickEncoder *AIR_Encoder;
ClickEncoder::Button Air_Button;
ClickEncoder::Button Tip_Button;

PID TIP_PID(&Tip_Input, &Tip_Output, &Tip_setPoint, Current_Config.TIPKP, Current_Config.TIPKI, Current_Config.TIPKD, DIRECT);
PID AIR_PID(&Air_Input, &Air_Output, &Air_setPoint, Current_Config.AIRKP, Current_Config.AIRKI, Current_Config.AIRKD, DIRECT);

//-------------------------------------------------------------------------------------

void setup() {

    loadConfig();
    Init_HW();
    LCD_Init();
    attachInterrupt(digitalPinToInterrupt(2), ISR_ZX, RISING);

}



void loop() {
  readTemperatures();
  doTipWork();
  doAirWork();
  refreshLCD();
  TIP_PID.Compute();
  AIR_PID.Compute();
} 


void doAirWork(){
  char btn;
  switch(Air_Estado){   //0:OFF 1:Working 2:Menu 3:AirSpeed 4: Cooling
    case 0:        if(getAirBtn()==CLICK){Air_Estado = 1;TurnAirOn();}
                   break;

    case 1:        Air_setPoint += AIR_Encoder->getValue();
                   if(Air_setPoint < MIN_AIR_WORK_TEMP){Air_setPoint = MIN_AIR_WORK_TEMP;}
                   if(Air_setPoint > MAX_AIR_WORK_TEMP){Air_setPoint = MAX_AIR_WORK_TEMP;}
                   AirWorkSetpoint = Air_setPoint;
                   btn  = getAirBtn();
                   if (btn==HELD){Air_Estado = 4;}
                   if (btn==CLICK){Air_Estado = 3;lcd.setCursor(0,3);lcd.print("                ");}
                   if (btn==DBLCLICK){if(Tip_Estado !=2){ Air_Estado = 2;}}
                   break;
                   
    case 2:       switch(Cur_Item){   // 0:NO ITEM 1:AIR_DEFAULT_T 2:AIR_DEFAULT_S 3:AIRKP 4:AIRKI 5:AIRKD

                  case 0:     Cur_Pos += AIR_Encoder->getValue();
                              if (Cur_Pos < 0){Cur_Pos = 0;}
                              if (Cur_Pos > 4){Cur_Pos = 4;}
                              btn = getAirBtn();  
                              if (btn==DBLCLICK){Air_Estado = 1;saveConfig();Menu_Page = 0;lcd.clear();}
                              if (btn==CLICK){Cur_Item = Cur_Pos+1;}
                              
                              break;
                  case 1:     Current_Config.AIR_DEFAULT_T += AIR_Encoder->getValue();
                              if (Current_Config.AIR_DEFAULT_T < MIN_AIR_WORK_TEMP){Current_Config.AIR_DEFAULT_T = MIN_AIR_WORK_TEMP;}
                              if (Current_Config.AIR_DEFAULT_T > MAX_AIR_WORK_TEMP){Current_Config.AIR_DEFAULT_T = MAX_AIR_WORK_TEMP;}
                              btn = getAirBtn();  
                              if (btn==CLICK){Cur_Item = 0;}
                              break;
                  case 2:     Current_Config.AIR_DEFAULT_S += AIR_Encoder->getValue();
                              if (Current_Config.AIR_DEFAULT_S < MIN_AIR_SPEED){Current_Config.AIR_DEFAULT_S = MIN_AIR_SPEED;}
                              if (Current_Config.AIR_DEFAULT_S > MAX_AIR_SPEED){Current_Config.AIR_DEFAULT_S = MAX_AIR_SPEED;}
                              btn = getAirBtn();  
                              if (btn==CLICK){Cur_Item = 0;}
                              break;
                  case 3:     Current_Config.AIRKP += AIR_Encoder->getValue();
                              if (Current_Config.AIRKP < MIN_KP){Current_Config.AIRKP = MIN_KP;}
                              if (Current_Config.AIRKP > MAX_KP){Current_Config.AIRKP = MAX_KP;}
                              btn = getAirBtn();  
                              if (btn==CLICK){Cur_Item = 0;}
                              break;
                  case 4:     Current_Config.AIRKI += float(float(AIR_Encoder->getValue())/1000);
                              if (Current_Config.AIRKI < MIN_KI){Current_Config.AIRKI = MIN_KI;}
                              if (Current_Config.AIRKI > MAX_KI){Current_Config.AIRKI = MAX_KI;}
                              btn = getAirBtn();  
                              if (btn==CLICK){Cur_Item = 0;}
                              break;
                  case 5:     Current_Config.AIRKD += float(float(AIR_Encoder->getValue())/10);
                              if (Current_Config.AIRKD < MIN_KD){Current_Config.AIRKD = MIN_KD;}
                              if (Current_Config.AIRKD > MAX_KD){Current_Config.AIRKD = MAX_KD;}
                              btn = getAirBtn(); 
                              if (btn==CLICK){Cur_Item = 0;}
                              break;                                                         
                              
                  }
                  break;

     case 3:      AirSpeed += AIR_Encoder->getValue();
                  if(AirSpeed < MIN_AIR_SPEED){AirSpeed = MIN_AIR_SPEED;}
                  if(AirSpeed > MAX_AIR_SPEED){AirSpeed = MAX_AIR_SPEED;}
                  btn  = getAirBtn();
                  if (btn==HELD){Air_Estado = 0;TurnAirOff();}
                  if (btn==CLICK){Air_Estado = 1;}
                  if (btn==DBLCLICK){if(Tip_Estado !=2){ Air_Estado = 2;}}
                  break;

     case 4:      TurnAirHeatOff();
                  if(Air_Temperature < 100){
                    TurnAirOff();
                    Air_Estado = 0;
                  }                          
                  btn  = getAirBtn();                  
                  if (btn==CLICK){Air_Estado = 1;}                
                  break;
}
}


void doTipWork(){
  char btn;
  switch(Tip_Estado){   //0:OFF 1:Working 2:Menu 3:IDLE
    case 0:        if(getTipBtn()==CLICK){Tip_Estado = 1;TurnTipOn();}
                   break;

    case 1:        Tip_setPoint += TIP_Encoder->getValue();
                   if(Tip_setPoint < MIN_TIP_WORK_TEMP){Tip_setPoint = MIN_TIP_WORK_TEMP;}
                   if(Tip_setPoint > MAX_TIP_WORK_TEMP){Tip_setPoint = MAX_TIP_WORK_TEMP;}
                   TipWorkSetpoint = Tip_setPoint;
                   btn  = getTipBtn();
                   if (btn==HELD){Tip_Estado = 0;TurnTipOff();}
                   if (btn==CLICK){Tip_Estado = 3;}
                   if (btn==DBLCLICK){if(Air_Estado !=2){ Tip_Estado = 2;}}
                   break;

    case 2:       switch(Cur_Item){   // 0:NO ITEM 1:TIP_IDLE_T 2:TIP_DEFAULT_T 3:TIPKP 4:TIPKI 5:TIPKD

                  case 0:     Cur_Pos += TIP_Encoder->getValue();
                              if (Cur_Pos < 0){Cur_Pos = 0;}
                              if (Cur_Pos > 4){Cur_Pos = 4;}
                              btn = getTipBtn();  
                              if (btn==DBLCLICK){Tip_Estado = 1;saveConfig();Menu_Page = 0;lcd.clear();}
                              if (btn==CLICK){Cur_Item = Cur_Pos+1;}
                              
                              break;
                  case 1:     Current_Config.TIP_IDLE_T += TIP_Encoder->getValue();
                              if (Current_Config.TIP_IDLE_T < MIN_IDLE_TEMP){Current_Config.TIP_IDLE_T = MIN_IDLE_TEMP;}
                              if (Current_Config.TIP_IDLE_T > MAX_IDLE_TEMP){Current_Config.TIP_IDLE_T = MAX_IDLE_TEMP;}
                              btn = getTipBtn();  
                              if (btn==CLICK){Cur_Item = 0;}
                              break;
                  case 2:     Current_Config.TIP_DEFAULT_T += TIP_Encoder->getValue();
                              if (Current_Config.TIP_DEFAULT_T < MIN_TIP_WORK_TEMP){Current_Config.TIP_DEFAULT_T = MIN_TIP_WORK_TEMP;}
                              if (Current_Config.TIP_DEFAULT_T > MAX_TIP_WORK_TEMP){Current_Config.TIP_DEFAULT_T = MAX_TIP_WORK_TEMP;}
                              btn = getTipBtn();  
                              if (btn==CLICK){Cur_Item = 0;}
                              break;
                  case 3:     Current_Config.TIPKP += TIP_Encoder->getValue();
                              if (Current_Config.TIPKP < MIN_KP){Current_Config.TIPKP = MIN_KP;}
                              if (Current_Config.TIPKP > MAX_KP){Current_Config.TIPKP = MAX_KP;}
                              btn = getTipBtn();  
                              if (btn==CLICK){Cur_Item = 0;}
                              break;
                  case 4:     Current_Config.TIPKI += float(float(TIP_Encoder->getValue())/1000);
                              if (Current_Config.TIPKI < MIN_KI){Current_Config.TIPKI = MIN_KI;}
                              if (Current_Config.TIPKI > MAX_KI){Current_Config.TIPKI = MAX_KI;}
                              btn = getTipBtn();  
                              if (btn==CLICK){Cur_Item = 0;}
                              break;
                  case 5:     Current_Config.TIPKD += float(float(TIP_Encoder->getValue())/10);
                              if (Current_Config.TIPKD < MIN_KD){Current_Config.TIPKD = MIN_KD;}
                              if (Current_Config.TIPKD > MAX_KD){Current_Config.TIPKD = MAX_KD;}
                              btn = getTipBtn(); 
                              if (btn==CLICK){Cur_Item = 0;}
                              break;                                                         
                              
                  }
                  break;

     case 3:      Tip_setPoint = Current_Config.TIP_IDLE_T;
                  btn = getTipBtn();
                  if (btn==HELD){Tip_Estado = 0;TurnTipOff();}
                  if (btn==CLICK){Tip_Estado = 1;Tip_setPoint = TipWorkSetpoint;}
                  break;

}
}



void loadConfig(){
  EEPROM.get(CONFIG_EEPROM_ADDR,Current_Config);
    if ( Current_Config.VALID != VALID_CONFIG){
      Current_Config.TIP_IDLE_T = DEFAULT_IDLE_TEMP;
      Current_Config.TIP_DEFAULT_T = DEFAULT_TIP_WORK_TEMP;
      Current_Config.AIR_DEFAULT_T = DEFAULT_AIR_WORK_TEMP;
      Current_Config.AIR_DEFAULT_S = DEFAULT_AIR_SPEED;
      Current_Config.TIPKP = DEFAULT_KP;
      Current_Config.TIPKI = DEFAULT_KI;
      Current_Config.TIPKD = DEFAULT_KD;
      Current_Config.AIRKP = DEFAULT_KP;
      Current_Config.AIRKI = DEFAULT_KI;
      Current_Config.AIRKD = DEFAULT_KD;
      Current_Config.VALID = VALID_CONFIG;
      EEPROM.put(CONFIG_EEPROM_ADDR,Current_Config);
    }
    Air_setPoint = Current_Config.AIR_DEFAULT_T;
    Tip_setPoint = Current_Config.TIP_DEFAULT_T;
    AirSpeed = Current_Config.AIR_DEFAULT_S;
}

void saveConfig(){
      EEPROM.put(CONFIG_EEPROM_ADDR,Current_Config);
      TIP_PID.SetTunings(Current_Config.TIPKP, Current_Config.TIPKI, Current_Config.TIPKD);
      AIR_PID.SetTunings(Current_Config.AIRKP, Current_Config.AIRKI, Current_Config.AIRKD);
}

void timerIsr() {
  RotaryTicks++;
  if (RotaryTicks >= 10){
    TIP_Encoder->service();
    AIR_Encoder->service();
  }
}

void ISR_ZX(){
  if (ZXTicks >= Tip_Output){ digitalWriteFast(TIP_HEAT_OUT,LOW);}
  if (ZXTicks >= Air_Output){ digitalWriteFast(AIR_HEAT_OUT,LOW);}
  if (AirTicks >= AirSpeed){ digitalWriteFast(AIR_SPEED_OUT,LOW);}  
 
  if (ZXTicks >= ZXCYCLE){
    if(Tip_Output > 0){ 
      digitalWriteFast(TIP_HEAT_OUT,HIGH);
    }
    if(Air_Output > 0){ 
      digitalWriteFast(AIR_HEAT_OUT,HIGH);
    }
    if(AirSpeed > 0){ 
      digitalWriteFast(AIR_SPEED_OUT,HIGH);  
    }  
    ZXTicks = 0;
    }
  ZXTicks++;
}