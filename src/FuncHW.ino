#include <Arduino.h>

/*
 * LCD RS pin to digital pin 12
 * LCD Enable pin to digital pin 11
 * LCD D4 pin to digital pin 10
 * LCD D5 pin to digital pin 9
 * LCD D6 pin to digital pin 8
 * LCD D7 pin to digital pin 7
 * DINRED   2
 * CS_AIR   53
 * CS_TIP   23
 * AIR_ENC_A  18
 * AIR_ENC_B  19
 * TIP_ENC_A  20
 * TIP_ENC_B  21
 * AIR_ENC_BUT  A3
 * TIP_ENC_BUT  A5
 * PUSH         A4
 * AIR_HEAT_OUT 47
 * AIR_SPEED_OUT 45
 * TIP_HEAT_OUT 49




 */

void Init_HW(){
  pinMode(CS_AIR,OUTPUT);
  pinMode(CS_TIP,OUTPUT);  
  pinMode(DINRED,INPUT);
  pinModeFast(AIR_HEAT_OUT,OUTPUT);
  pinModeFast(AIR_SPEED_OUT,OUTPUT);
  pinModeFast(TIP_HEAT_OUT,OUTPUT);
  
  TIP_Encoder = new ClickEncoder(TIP_ENC_A, TIP_ENC_B, TIP_ENC_BUT);
  AIR_Encoder = new ClickEncoder(AIR_ENC_A, AIR_ENC_B, AIR_ENC_BUT);
  TIP_Encoder->setAccelerationEnabled(false);
  AIR_Encoder->setAccelerationEnabled(false);
  TIP_Encoder->setButtonHeldEnabled(true);
  TIP_Encoder->setDoubleClickEnabled(true);
  AIR_Encoder->setButtonHeldEnabled(true);
  AIR_Encoder->setDoubleClickEnabled(true);
  TIP_PID.SetOutputLimits(0, 100);
  TIP_PID.SetSampleTime(500);
  TIP_PID.SetMode(MANUAL);
  TIP_PID.SetTunings(Current_Config.TIPKP, Current_Config.TIPKI, Current_Config.TIPKD);
  AIR_PID.SetOutputLimits(0, 100);
  AIR_PID.SetSampleTime(500);
  AIR_PID.SetMode(MANUAL);
  AIR_PID.SetTunings(Current_Config.AIRKP, Current_Config.AIRKI, Current_Config.AIRKD);
  TurnAirOff();
  Timer1.initialize(1000);
  Timer1.attachInterrupt(timerIsr); 
}


void readTemperatures(){
  Tip_Temperature = KtoJ(TIP_TC.readCelsius());
  Tip_Input = Tip_Temperature;
  Air_Temperature = KtoJ(AIR_TC.readCelsius());
  Air_Input = Air_Temperature;
}

double KtoJ(double Temp){
  double Jtemp=0;
  Jtemp=0.3797+0.765*Temp+0.000001*Temp*Temp-0.00000005*Temp*Temp*Temp;
  return Jtemp;
}

char getAirBtn(){
  Air_Button = AIR_Encoder->getButton();
  if (Air_Button != ClickEncoder::Open) {
    switch (Air_Button) {
        case ClickEncoder::DoubleClicked:     
                                        return DBLCLICK;
                                        break;
        case ClickEncoder::Clicked:     
                                        return CLICK;
                                        break;
        case ClickEncoder::Released:     
                                        return RELEASED;
                                        break;                                                                                                             
        case ClickEncoder::Held:    
                                        return HELD;     
                                        break;  
        default:
                                        return OPEN;
                                        break;
    }
  }else{
    return OPEN;
  }
} 

char getTipBtn(){
  Tip_Button = TIP_Encoder->getButton();
  if (Tip_Button != ClickEncoder::Open) {
    switch (Tip_Button) {
        case ClickEncoder::DoubleClicked:     
                                        return DBLCLICK;
                                        break;
        case ClickEncoder::Clicked:     
                                        return CLICK;
                                        break;
        case ClickEncoder::Released:     
                                        return RELEASED;
                                        break;                                                                                                             
        case ClickEncoder::Held:    
                                        return HELD;     
                                        break;
        default:                        
                                        return OPEN;
                                        break;
    }
  }else{
    return OPEN;
  }
} 


void TurnTipOn(){
      TIP_PID.SetMode(AUTOMATIC);
}

void TurnTipOff(){
      TIP_PID.SetMode(MANUAL);
      Tip_Output = 0;
}

void TurnAirOn(){
  AIR_PID.SetMode(AUTOMATIC);
  AirSpeed = Current_Config.AIR_DEFAULT_S;
}

void TurnAirOff(){
  AIR_PID.SetMode(MANUAL);
  Air_Output = 0;
  AirSpeed = 0;
}

void TurnAirHeatOff(){
  AIR_PID.SetMode(MANUAL);
  Air_Output = 0;
}