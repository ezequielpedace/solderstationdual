#include <Arduino.h>

byte customChar[8] = {0b01100,0b10010,0b10010,0b01100,0b00000,0b00000,0b00000,0b00000};



void LCD_Init(){
lcd.begin(16, 4);  
lcd.createChar(1, customChar); 
lcd.setCursor(0,0);
}


void refreshLCD(){
  if (Air_Estado !=2){
  switch(Tip_Estado){  //0:OFF 1:Working 2:Menu 3:IDLE
    case 0:     lcd.setCursor(0,0);
                lcd.print("    TIP OFF     ");
                lcd.setCursor(0,1);
                lcd.print("                ");      
                break;                 
    case 1:     lcd.setCursor(0,0);
                lcd.print("TIP WORK  O:");
                temp=String(int(Tip_Output));
                if (temp.length() == 1){temp = "00"+temp;}else if(temp.length() == 2){temp = "0"+temp;}
                lcd.print(temp);
                lcd.print('%');
                lcd.setCursor(0,1);
                if (Tip_Temperature == NAN){
                lcd.print("TC ERROR");
                }else{
                lcd.print("T:");
                temp = String(int(Tip_Temperature));
                if (temp.length() == 1){temp = "00"+temp;}else if(temp.length() == 2){temp = "0"+temp;}
                lcd.print(temp);
                lcd.write(1);
                lcd.print(" >> S:");
                temp = String(int(Tip_setPoint));
                if (temp.length() == 1){temp = "00"+temp;}else if(temp.length() == 2){temp = "0"+temp;}
                lcd.print(temp);
                lcd.write(1);
                }                 
                break;
    case 2:     switch(Cur_Item){  // 0:NO ITEM 1:TIP_IDLE_T 2:TIP_DEFAULT_T 3:TIPKP 4:TIPKI 5:TIPKD
                   case 0:      //lcd.clear();
                                if(Cur_Pos <= 3){
                                  if(Menu_Page != 1){lcd.clear();Menu_Page = 1;}
                                  lcd.setCursor(0,0);
                                  lcd.print(" ");
                                  lcd.setCursor(0,1);
                                  lcd.print(" ");
                                  lcd.setCursor(0,2);
                                  lcd.print(" ");
                                  lcd.setCursor(0,3);
                                  lcd.print(" ");
                                  lcd.setCursor(0,Cur_Pos);
                                  lcd.write(199);
                                  lcd.setCursor(1,0);        
                                  lcd.print("IDLE TEMP ");
                                  lcd.print(Current_Config.TIP_IDLE_T);
                                  lcd.write(1);
                                  lcd.setCursor(1,1);        
                                  lcd.print("DEF TEMP  ");
                                  lcd.print(Current_Config.TIP_DEFAULT_T);
                                  lcd.write(1);
                                  lcd.setCursor(1,2);        
                                  lcd.print("KP        ");
                                  lcd.print(Current_Config.TIPKP,0);
                                  
                                  lcd.setCursor(1,3);        
                                  lcd.print("KI        ");
                                  lcd.print(Current_Config.TIPKI,3);
                                                                    
                                }else{
                                  if(Menu_Page != 2){lcd.clear();Menu_Page = 2;}
                                  lcd.setCursor(0,0);
                                  lcd.write(199);
                                  lcd.setCursor(1,0);        
                                  lcd.print("KD        ");
                                  lcd.print(Current_Config.TIPKD,1);
                                                                    
                                }
                                                                       
                                break;

                   case 1:      lcd.setCursor(0,Cur_Pos);
                                lcd.write(188);
                                lcd.setCursor(11,0);
                                temp = String(int(Current_Config.TIP_IDLE_T));
                                if (temp.length() == 1){temp = "00"+temp;}else if(temp.length() == 2){temp = "0"+temp;}
                                lcd.print(temp);
                                lcd.write(1);            
                                break;
                   case 2:      lcd.setCursor(0,Cur_Pos);
                                lcd.write(188);
                                lcd.setCursor(11,1);        
                                temp = String(int(Current_Config.TIP_DEFAULT_T));
                                if (temp.length() == 1){temp = "00"+temp;}else if(temp.length() == 2){temp = "0"+temp;}
                                lcd.print(temp);
                                lcd.write(1);            
                                break;
                   case 3:      lcd.setCursor(0,Cur_Pos);
                                lcd.write(188);
                                lcd.setCursor(11,2);           
                                lcd.print(Current_Config.TIPKP,0);          
                                break;                                      
                   case 4:      lcd.setCursor(0,Cur_Pos);
                                lcd.write(188);
                                lcd.setCursor(11,3);        
                                lcd.print(Current_Config.TIPKI,3);
                                            
                                break;                            
                   case 5:      lcd.setCursor(0,0);
                                lcd.write(188);
                                lcd.setCursor(11,0);        
                                lcd.print(Current_Config.TIPKD,1);
                                break;                 
                 }
                 break;
    case 3:      lcd.setCursor(0,0);
                  lcd.print("TIP IDLE  O:");
                 temp = String(int(Tip_Output));
                 if (temp.length() == 1){temp = "00"+temp;}else if(temp.length() == 2){temp = "0"+temp;}
                 lcd.print(temp);
                 lcd.print('%');
                 lcd.setCursor(0,1);
                 if (Tip_Temperature == NAN){
                 lcd.print("TC ERROR");
                 }else{
                  lcd.print("T:");
                  temp = String(int(Tip_Temperature));
                  if (temp.length() == 1){temp = "00"+temp;}else if(temp.length() == 2){temp = "0"+temp;}
                  lcd.print(temp);
                  lcd.write(1);
                  lcd.print(" >> S:");
                  temp = String(int(Tip_setPoint));
                  if (temp.length() == 1){temp = "00"+temp;}else if(temp.length() == 2){temp = "0"+temp;}
                  lcd.print(temp);
                  lcd.write(1);
                  }                 
                  break;
  }}
  if(Tip_Estado !=2){
  switch(Air_Estado){  //0:OFF 1:Working 2:Menu 3:Speed
    case 0:     lcd.setCursor(0,2);
                lcd.print("    AIR OFF     ");
                lcd.setCursor(0,3);
                lcd.print("                ");      
                break;                 
    case 1:     lcd.setCursor(0,2);
                lcd.print("AIR WORK  O:");
                temp=String(int(Air_Output));
                if (temp.length() == 1){temp = "00"+temp;}else if(temp.length() == 2){temp = "0"+temp;}
                lcd.print(temp);
                lcd.print('%');
                lcd.setCursor(0,3);
                if (Air_Temperature == NAN){
                lcd.print("AIR TC ERROR");
                }else{
                lcd.print("T:");
                temp = String(int(Air_Temperature));
                if (temp.length() == 1){temp = "00"+temp;}else if(temp.length() == 2){temp = "0"+temp;}
                lcd.print(temp);
                lcd.write(1);
                lcd.print(" >> S:");
                temp = String(int(Air_setPoint));
                if (temp.length() == 1){temp = "00"+temp;}else if(temp.length() == 2){temp = "0"+temp;}
                lcd.print(temp);
                lcd.write(1);
                }                 
                break;
    case 2:     switch(Cur_Item){  // 0:NO ITEM 1:AIR_DEFAULT_T 2:AIR_DEFAULT_S 3:AIRKP 4:AIRKI 5:AIRKD
                   case 0:      
                                if(Cur_Pos <= 3){
                                  if(Menu_Page != 1){lcd.clear();Menu_Page = 1;}
                                  lcd.setCursor(0,0);
                                  lcd.print(" ");
                                  lcd.setCursor(0,1);
                                  lcd.print(" ");
                                  lcd.setCursor(0,2);
                                  lcd.print(" ");
                                  lcd.setCursor(0,3);
                                  lcd.print(" ");
                                  lcd.setCursor(0,Cur_Pos);
                                  lcd.write(199);
                                  lcd.setCursor(1,0);        
                                  lcd.print("DEF TEMP  ");
                                  lcd.print(Current_Config.AIR_DEFAULT_T);
                                  lcd.write(1);
                                  lcd.setCursor(1,1);        
                                  lcd.print("SPEED     ");
                                  lcd.print(Current_Config.AIR_DEFAULT_S);
                                  lcd.print("%");
                                  lcd.setCursor(1,2);        
                                  lcd.print("KP        ");
                                  lcd.print(Current_Config.AIRKP,0);
                                  
                                  lcd.setCursor(1,3);        
                                  lcd.print("KI        ");
                                  lcd.print(Current_Config.AIRKI,3);
                                                                    
                                }else{
                                  if(Menu_Page != 2){lcd.clear();Menu_Page = 2;}
                                  lcd.setCursor(0,0);
                                  lcd.write(199);
                                  lcd.setCursor(1,0);        
                                  lcd.print("KD        ");
                                  lcd.print(Current_Config.AIRKD,1);
                                                                    
                                }
                                                                       
                                break;
                   case 1:      lcd.setCursor(0,Cur_Pos);
                                lcd.write(188);
                                lcd.setCursor(11,0);        
                                temp = String(int(Current_Config.AIR_DEFAULT_T));
                                if (temp.length() == 1){temp = "00"+temp;}else if(temp.length() == 2){temp = "0"+temp;}
                                lcd.print(temp);
                                lcd.write(1);            
                                break;
                   case 2:      lcd.setCursor(0,Cur_Pos);
                                lcd.write(188);
                                lcd.setCursor(11,1);        
                                temp = String(int(Current_Config.AIR_DEFAULT_S));
                                if (temp.length() == 1){temp = "00"+temp;}else if(temp.length() == 2){temp = "0"+temp;}
                                lcd.print(temp);
                                lcd.print("%");         
                                break;
                   case 3:      lcd.setCursor(0,Cur_Pos);
                                lcd.write(188);
                                lcd.setCursor(11,2);           
                                lcd.print(Current_Config.AIRKP,0);
                                            
                                break;                                      
                   case 4:      lcd.setCursor(0,Cur_Pos);
                                lcd.write(188);
                                lcd.setCursor(11,3);        
                                lcd.print(Current_Config.AIRKI,3);
                                            
                                break;                            
                   case 5:      lcd.setCursor(0,0);
                                lcd.write(188);
                                lcd.setCursor(11,0);        
                                lcd.print(Current_Config.AIRKD,1);
                                break;                 
                 }
                 break;
    case 3:       lcd.setCursor(0,2);
                  lcd.print("    AIR SPEED   ");

                  lcd.setCursor(0,3);
                  temp = String(int(AirSpeed));
                  if (temp.length() == 1){temp = "00"+temp;}else if(temp.length() == 2){temp = "0"+temp;}
                  lcd.print(temp);
                  lcd.print('%');
                  break;
                  
    case 4:       lcd.setCursor(0,2);
                  lcd.print("Air Cooling...  ");
                  lcd.setCursor(0,3);
                  if (Air_Temperature == NAN){
                  lcd.print("AIR TC ERROR");
                  }else{
                  lcd.print("T:");
                  temp = String(int(Air_Temperature));
                  if (temp.length() == 1){temp = "00"+temp;}else if(temp.length() == 2){temp = "0"+temp;}
                  lcd.print(temp);
                  lcd.write(1);
                  lcd.print(" >> S:");
                  temp = String(int(100));
                  if (temp.length() == 1){temp = "00"+temp;}else if(temp.length() == 2){temp = "0"+temp;}
                  lcd.print(temp);
                  lcd.write(1);
                  }                 
                  break;
  }
}
}


